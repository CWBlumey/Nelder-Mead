import scalaz.Scalaz._

case class Point(inner: List[Double]){
  val dimension: Int = inner.size

  def +(that: Point): Point = {
    assert(this.dimension == that.dimension, "Cannot add points of differing dimensions")
    Point((this.inner, that.inner).zipped.map(_+_))
  }

  def unary_-(): Point =
    Point(inner.map(-_))

  def -(that: Point): Point =
    this + (- that)

  def *(scalar: Double): Point =
    Point(inner.map(scalar*_))

  def /(scalar: Double): Point =
    this * (1 / scalar)
}

object Origin{
  def In(dimension: Int): Point =
    Point(List.fill(dimension)(0))
}

case class Bound(lower: Double, upper: Double){
  assert(upper > lower,
    "Upper bound (" + upper + ") must be greater than lower bound (" + lower+")")

  val range: Double = upper - lower
  val center: Double = upper + lower / 2

  def scaleRange(scalar: Double): Bound = {
    assert(scalar > 0, "scalar must be greater than 0")
    Bound(center + (range * scalar / 2), center - (range * scalar / 2))
  }
}

object Bound{
  def FromTuple(tuple: (Double, Double)): Bound =
    tuple match {
      case (lower, upper) => Bound(lower, upper)
    }
}

class NelderMead(val reflectionCoefficient: Double,
                 val expansionCoefficient: Double,
                 val contractionCoefficient: Double,
                 val shrinkageCoefficient: Double){

  assert(reflectionCoefficient >= 1,
    "reflectionCoefficient must be at least 1, it was: " + reflectionCoefficient)
  assert(expansionCoefficient >= 1,
    "expansionCoeffiicent must be at least 1, it was: " + expansionCoefficient)
  assert(contractionCoefficient < 1 && contractionCoefficient > 0,
    "contractionCoefficient must be less than 1 and greater than 0, it was: " + contractionCoefficient)
  assert(shrinkageCoefficient < 1 && shrinkageCoefficient > 0,
    "shrinkageCoefficient must be less than 1 and greater than 0, it was: " + shrinkageCoefficient)

  private case class NelderMeadInstance(fn: List[Double] => Double, bounds: List[Bound], changeThreshhold: Double){
    val dimension: Int = bounds.size
    val numberOfPoints: Int = dimension + 1

    protected case class Candidate(point: Point, score: Double) { }

    protected def pointToCandidate(point: Point): Candidate =
      Candidate(point, score(point))

    protected def candidateToPoint(candidate: Candidate): Point =
      candidate.point

    def score(point: Point): Double =
      fn(point.inner)

    def scaleDistanceFromPoint(scalar: Double)(reference: Point, other: Point): Point =
      reference + ((other - reference) * scalar)

    def reflectAroundCentroid: (Point, Point) => Point = scaleDistanceFromPoint(-reflectionCoefficient)

    def expandFromCentroid: (Point, Point) => Point = scaleDistanceFromPoint(expansionCoefficient)

    def contractTowardsCentroid: (Point, Point) => Point = scaleDistanceFromPoint(contractionCoefficient)

    def shrinkTowardsCentroid: (Point, Point) => Point = scaleDistanceFromPoint(shrinkageCoefficient)

    def returnIfBetterThan(threshhold: Double)(candidate: Candidate): Option[Candidate] =
      if (candidate.score > threshhold) Some(candidate)
      else None

    def recommendNewPointFrom(generateNewPoint: (Point, Point) => Point)(centroid: Point, original: Candidate): Option[Candidate] =
      original.point |>
        (generateNewPoint(centroid, _)) |>
        pointToCandidate |>
        returnIfBetterThan(original.score)

    def reflectionStep: (Point, Candidate) => Option[Candidate] = recommendNewPointFrom(reflectAroundCentroid)

    def expansionStep: (Point, Candidate) => Option[Candidate] = recommendNewPointFrom(expandFromCentroid)

    def contractionStep: (Point, Candidate) => Option[Candidate] = recommendNewPointFrom(contractTowardsCentroid)

    def shrinkageStep(centroid: Point, currentCandidates: List[Candidate]): List[Candidate] =
      currentCandidates
        .map(candidateToPoint)
        .map(shrinkTowardsCentroid(centroid, _))
        .map(pointToCandidate)

    def selectInitialCenter(bounds: List[Bound]): Point =

  }
}